%define debug_package %{nil}

Name:          helm
Version:       2.14.0
Release:       1%{?dist}
Summary:       The package manager for Kubernetes
License:       Apache 2.0
Url:           https://github.com/helm/helm
Source0:       %{url}/archive/v%{version}.tar.gz
BuildRequires: golang glide

%description
Helm helps you manage Kubernetes applications — Helm Charts helps you define,
install, and upgrade even the most complex Kubernetes application.

Charts are easy to create, version, share, and publish — so start using Helm and
stop the copy-and-paste.

The latest version of Helm is maintained by the CNCF - in collaboration with
Microsoft, Google, Bitnami and the Helm contributor community.

%prep
%setup -q -n %{name}-%{version}

%build
export GOPATH=$PWD
mkdir -p src/k8s.io/helm
shopt -s extglob dotglob
mv !(src) src/k8s.io/helm
shopt -u extglob dotglob
pushd src/k8s.io/helm/
make bootstrap build
popd

%install
install -d -m 755 $RPM_BUILD_ROOT%{_bindir}
install -m 755 src/k8s.io/helm/bin/helm $RPM_BUILD_ROOT%{_bindir}
install -m 755 src/k8s.io/helm/bin/rudder $RPM_BUILD_ROOT%{_bindir}
install -m 755 src/k8s.io/helm/bin/tiller $RPM_BUILD_ROOT%{_bindir}

%files
%license src/k8s.io/helm/LICENSE
%doc src/k8s.io/helm/{OWNERS,CONTRIBUTING.md,README.md,SECURITY_CONTACTS,code-of-conduct.md}
%{_bindir}/helm
%{_bindir}/rudder
%{_bindir}/tiller

%changelog
